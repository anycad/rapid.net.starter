using AnyCAD.Forms;
using AnyCAD.Foundation;

namespace WinFormsStarter
{
    public partial class Form1 : Form
    {
        RenderControl mRenderView;
        int mIdx = 0;
        List<EnumStandardView> mViewList = new List<EnumStandardView>();
        public Form1()
        {
            InitializeComponent();
            mRenderView = new RenderControl(this.splitContainer1.Panel2);

            mViewList.Add(EnumStandardView.Front);
            mViewList.Add(EnumStandardView.Top);
            mViewList.Add(EnumStandardView.DefaultView);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var shape = ShapeBuilder.MakeCylinder(new GAx2(), 10, 20, 0);
            mRenderView.ShowShape(shape, ColorTable.PaleVioletRed);
        }

        void NextPosition()
        {
            ++mIdx;
            if(mIdx >= mViewList.Count)
            {
                return;
            }

            mRenderView.SetStandardView(mViewList[mIdx], false);
            ScreenShot();
        }

        void ScreenShot()
        {
            mRenderView.SetAfterRenderingCallback(() =>
            {
                var ss = mRenderView.CreateScreenShot();
                ss.SaveFile($"d:/xx{mIdx}.png");
                mRenderView.SetAfterRenderingCallback(null);
                Invoke(new Action(() =>
                {
                    NextPosition();
                }));
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mIdx = 0;
            mRenderView.SetStandardView(mViewList[mIdx], false);

            ScreenShot();
        }
    }
}