﻿// See https://aka.ms/new-console-template for more information

using AnyCAD.Foundation;

GlobalInstance.Initialize();

if (args.Length == 0)
{
    Console.WriteLine("缺少文件路径参数");
    return;
}
var filePath = args[0];
var shape = ShapeIO.Open(filePath);
if (shape == null)
{
    Console.WriteLine("导入模型失败");
    return;
}
var bbox = shape.GetBBox();
GAx2[] axes =
{
    new GAx2(bbox.CornerMax().Translated(new GVec(10, 0, 0)), new GDir(new GVec(-1, 0, 0))),
    new GAx2(bbox.CornerMax().Translated(new GVec(0, 10, 0)), new GDir(new GVec(0, -1, 0))),
    new GAx2(bbox.CornerMax().Translated(new GVec(0, 0, 10)), new GDir(new GVec(0, 0, -1))),
};

var index = 0;
foreach (var ax in axes)
{
    ShapeHLRBuilder builder = new ShapeHLRBuilder();
    builder.AddShape(shape);
    if (builder.Compute(ax))
    {
        var edges = builder.GetVisibleOutlines();
        var group = ShapeBuilder.MakeCompound(edges);
        var hlrNode = BrepSceneNode.Create(group);
        hlrNode.SetColor(ColorTable.Yellow);

        var window3d = RenderingEngine.CreateWindow3D("AnyCAD", 1024, 768, false);
        window3d.ShowCoordinateGrid(false);
        var scene = window3d.GetContext().GetScene();
        scene.AddNode(hlrNode);
        window3d.ZoomToBox(hlrNode.GetBoundingBox());
        var tick = window3d.GetTimeTicks();
        var ret = window3d.Redraw(tick);

        var ss = window3d.CreateScreenShot();
        ss.SaveFile($"d:/xx{index++}.png");
        window3d.Destroy();
    }
}

GlobalInstance.Destroy();
